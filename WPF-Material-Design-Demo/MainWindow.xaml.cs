﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;

namespace WPF_Material_Design_Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateTime timeStart = new DateTime();
        DateTime timeNow = new DateTime();

        int buttonState = 0;
        Thread thread;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            switch (buttonState)
            {
                case 0 : {
                        timeStart = DateTime.Now;
                        buttonState = 1;
                        thread = new Thread(UpdateText);
                        thread.Start();
                        break;
                    }
                case 1:
                    {
                        buttonState = 2;
                        thread.Suspend();
                        break;
                    }
                case 2:
                    {
                        buttonState = 1;
                        thread.Resume();
                        break;
                    }
                default:
                    break;
            }
            
        }

        private void UpdateText()
        {
            while (true)
            {
                Thread.Sleep(TimeSpan.FromMilliseconds(10));
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    timeNow = DateTime.Now;
                    TimeSpan elapsed = timeNow - timeStart;
                    lblTime.Content = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                        elapsed.Hours,
                        elapsed.Minutes,
                        elapsed.Seconds,
                        elapsed.Milliseconds/10
                        );
                }), DispatcherPriority.Background);
            }
        }
        
    }
}
